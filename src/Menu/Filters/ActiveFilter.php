<?php

namespace rezashams1\rtl_adminltev3\Menu\Filters;

use rezashams1\rtl_adminltev3\Menu\ActiveChecker;
use rezashams1\rtl_adminltev3\Menu\Builder;

class ActiveFilter implements FilterInterface
{
    private $activeChecker;

    public function __construct(ActiveChecker $activeChecker)
    {
        $this->activeChecker = $activeChecker;
    }

    public function transform($item, Builder $builder)
    {
        if (! isset($item['header'])) {
            $item['active'] = $this->activeChecker->isActive($item);
        }

        return $item;
    }
}
