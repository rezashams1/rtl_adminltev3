<?php

namespace rezashams1\rtl_adminltev3\Menu\Filters;

use rezashams1\rtl_adminltev3\Menu\Builder;

interface FilterInterface
{
    public function transform($item, Builder $builder);
}
