<?php

namespace rezashams1\rtl_adminltev3;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Events\Dispatcher;
use rezashams1\rtl_adminltev3\Events\BuildingMenu;
use rezashams1\rtl_adminltev3\Menu\Builder;

class rtl_adminltev3
{

    protected $menu;
    protected $filters;
    protected $events;
    protected $container;

    public function __construct(
        array $filters,
        Dispatcher $events,
        Container $container
    ) {
        $this->filters = $filters;
        $this->events = $events;
        $this->container = $container;
    }

    public function menu()
    {
        if (! $this->menu) {
            $this->menu = $this->buildMenu();
        }
        return $this->menu;
    }

    protected function buildMenu()
    {
        $builder = new Builder($this->buildFilters());
        if (method_exists($this->events, 'dispatch')) {
            $this->events->dispatch(new BuildingMenu($builder));
        } else {
            $this->events->fire(new BuildingMenu($builder));
        }
        return $builder->menu;
    }

    protected function buildFilters()
    {
        return array_map([$this->container, 'make'], $this->filters);
    }

}
