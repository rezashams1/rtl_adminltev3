<?php

namespace rezashams1\rtl_adminltev3;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use rezashams1\rtl_adminltev3\Events\BuildingMenu;
use rezashams1\rtl_adminltev3\Http\ViewComposers\RTLAdminLteComposer;

class RTLAdminLTEServiceProvider extends BaseServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(rtl_adminltev3::class, function (Container $app) {
            return new rtl_adminltev3(
                $app['config']['rtl_adminltev3.filters'],
                $app['events'],
                $app
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Factory $view, Dispatcher $events, Repository $config)
    {
        $this->publishes([
            __DIR__.'/../dist' => public_path('vendor/rezashams/rtl_adminltev3/dist'),
        ], 'public');
        $this->publishes([
            __DIR__.'/../config/adminlte.php' => config_path('rtl_adminltev3.php'),
        ], 'public');
        $this->loadViews();
        $this->loadConfig();
        $this->registerViewComposers($view);
        static::registerMenu($events, $config);
        $this->loadMigrationsFrom(__DIR__.'/Migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    private function loadViews()
    {
        $viewsPath = $this->packagePath('resources/views');
        $emailsPath = $this->packagePath('resources/views/emails');
        $this->loadViewsFrom($viewsPath, 'rtl_adminltev3');
        $this->loadViewsFrom($emailsPath, 'rtl_adminltev3_email');
    }

    private function packagePath($path)
    {
        return __DIR__."/../$path";
    }

    private function loadConfig()
    {
        $configPath = $this->packagePath('config/adminlte.php');
        $authPath = $this->packagePath('config/auth.php');
        $providerPath = $this->packagePath('config/provider.php');

        $this->mergeConfigFrom($configPath, 'rtl_adminltev3');
        $this->mergeConfigFrom($authPath, 'auth.guards');
        $this->mergeConfigFrom($providerPath, 'auth.providers');
    }

    private function registerViewComposers(Factory $view)
    {
        $view->composer('rtl_adminltev3::page', RTLAdminLteComposer::class);
    }

    public static function registerMenu(Dispatcher $events, Repository $config)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) use ($config) {
            $menu = $config->get('rtl_adminltev3.menu');
            call_user_func_array([$event->menu, 'add'], $menu);
        });
    }

}
