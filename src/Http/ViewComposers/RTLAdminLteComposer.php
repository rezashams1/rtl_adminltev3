<?php

namespace rezashams1\rtl_adminltev3\Http\ViewComposers;

use Illuminate\View\View;
use rezashams1\rtl_adminltev3\rtl_adminltev3;

class RTLAdminLteComposer
{

    /**
     * @var rtl_adminltev3
     */

    private $rtl_adminltev3;

    public function __construct(rtl_adminltev3 $rtl_adminltev3)
    {
        $this->rtl_adminltev3 = $rtl_adminltev3;
    }

    public function compose(View $view)
    {
        $view->with('rtl_adminltev3', $this->rtl_adminltev3);
    }

}
