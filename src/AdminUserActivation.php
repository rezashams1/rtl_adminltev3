<?php

namespace rezashams1\rtl_adminltev3;

use Illuminate\Database\Eloquent\Model;

class AdminUserActivation extends Model
{

    protected $table = "admin_users_activations";

    function adminUser() {

        return $this->belongsTo('rezashams1\rtl_adminltev3\AdminUser');

    }

}
