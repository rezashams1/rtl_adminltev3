<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use rezashams1\rtl_adminltev3\Http\Rules\CustomRule;

Route::middleware(['web'])->group(function () {

    Route::post(config('rtl_adminltev3.do_login_url', 'rtl_adminltev3/do-login'), function (Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ], [
            'email.required' => 'ایمیل خود را وارد کنید',
            'email.emails' => 'ایمیل وارد شده نامعتبر است',
            'password.required' => 'رمز عبور خود را وارد کنید'
        ]);

        if($validator->fails()) {

            return back()->withErrors($validator->errors())->withInput($request->all());

        } else {

            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {

                return redirect()->to(config('rtl_adminltev3.dashboard_url', 'admin/dashboard'));

            } else {

                $validator = Validator::make($request->all(), [
                    'email' => [new CustomRule('اطلاعات وارد شده نامعتبر است')],
                    'password' => [new CustomRule('اطلاعات وارد شده نامعتبر است')],
                ]);

                return back()->withErrors($validator->errors())->withInput($request->all());

            }

        }

    });

    Route::post(config('rtl_adminltev3.do_password_forget_url', 'rtl_adminltev3/do-forget-password'), function (Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ], [
            'email.required' => 'ایمیل خود را وارد کنید',
            'email.email' => 'ایمیل وارد شده نامعتبر است'
        ]);

        if($validator->fails()) {

            return back()->withErrors($validator->errors())->withInput($request->all());

        } else {

            $admin_user = \rezashams1\rtl_adminltev3\AdminUser::where('email', $request->email);
            if ($admin_user->get()->count() == 1) {

                \rezashams1\rtl_adminltev3\AdminUserActivation::where('admin_user_id', $admin_user->first()->id)
                    ->where('name', 'password_reset')
                    ->delete();

                $new_code = \Illuminate\Support\Str::uuid()->toString();

                try {

                    $insert_code = \rezashams1\rtl_adminltev3\AdminUserActivation::insert([
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'admin_user_id' => $admin_user->first()->id,
                        'name' => 'password_reset',
                        'value' => $new_code
                    ]);

                    if($insert_code) {

                        $emailContent = new stdClass();
                        $emailContent->confirm_code = $new_code;
                        \Illuminate\Support\Facades\Mail::to($admin_user->first()->email)->send(new \rezashams1\rtl_adminltev3\Mail\ForgetPasswordEmail(config('rtl_adminltev3.email_sender', 'noreply@reza-shams.ir'), $emailContent));

                        try {

                            if(count(\Illuminate\Support\Facades\Mail::failures()) > 0) {

                                return back()->with(['success' => false, 'message' => 'مشکلی در اتصال به سرور رخ داد. لطفا دوباره تلاش کتید']);

                            } else {

                                return back()->with(['success' => true, 'message' => 'ایمیل حاوی لینک تغییر رمز عبور برای شما ارسال گردید']);

                            }

                        } catch (\Exception $e) {

                            return back()->with(['success' => false, 'message' => 'مشکلی در اتصال به سرور رخ داد. لطفا دوباره تلاش کتید']);

                        }

                    } else {

                        return back()->with(['success' => false, 'message' => 'مشکلی در اتصال به سرور رخ داد. لطفا دوباره تلاش کتید']);

                    }

                } catch (\Illuminate\Database\QueryException $e) {

                    return back()->with(['success' => false, 'message' => 'مشکلی در اتصال به سرور رخ داد. لطفا دوباره تلاش کتید']);

                }

            } else {

                $validator = Validator::make($request->all(), [
                    'email' => [new CustomRule('اطلاعات وارد شده نامعتبر است')]
                ]);

                return back()->withErrors($validator->errors())->withInput($request->all());

            }

        }

    });

    Route::get(config('rtl_adminltev3.password_reset_url', 'admin/reset-password') . '/{confirm_code}', function (Request $request) {

        $errors = new stdClass();

        $confirm_code = $request->confirm_code;
        $user_id = 0;

        $find_confirm_code = AdminUserActivation::where('name', 'password_reset')->where('value', $confirm_code);
        if($find_confirm_code->get()->count() == 1) {

            $user_id = $find_confirm_code->first()->admin_user_id;

        } else {

            $errors->invalid_code = "آدرس نامعتبر است";

        }

        return view('rtl_adminltev3::reset_password')->with(['errors' => $errors, 'user_id' => $user_id]);

    });

    Route::post(config('rtl_adminltev3.do_password_reset_url', 'rtl_adminltev3/do-reset-password'), function (Request $request) {

        $validator = Validator::make($request->all(), [
            'password' => 'required'
        ], [
            'password.required' => 'رمز عبور جدید را وارد کنید'
        ]);

        if($validator->fails()) {

            return back()->withErrors($validator->errors())->withInput($request->all());

        } else {

            if(true) {

                try {

                    $update_password = \rezashams1\rtl_adminltev3\AdminUser::where('id', $request->user_id)->update([
                        'password' => \Illuminate\Support\Facades\Hash::make($request->password)
                    ]);

                    if($update_password) {

                        return back()->with(['success' => true, 'message' => 'رمز عبور شما با موفقیت تغییر کرد']);

                    } else {

                        return back()->with(['success' => false, 'message' => 'مشکلی در اتصال به سرور رخ داد. لطفا دوباره تلاش کتید']);

                    }

                } catch (\Illuminate\Database\QueryException $e) {

                    return back()->with(['success' => false, 'message' => 'مشکلی در اتصال به سرور رخ داد. لطفا دوباره تلاش کتید']);

                }

            }

        }

    });

    Route::get(config('rtl_adminltev3.logout_url', 'admin/logout'), function () {

        if(Auth::guard('admin')->check()) {

            Auth::guard('admin')->logout();

        }

        return redirect()->to('/');

    });

});
