<?php

namespace rezashams1\rtl_adminltev3\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $from_mail;
    public $email_content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from_mail, $email_content)
    {
        $this->from_mail = $from_mail;
        $this->email_content = $email_content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->from_mail)->view('rtl_adminltev3_email::forget_password')->with(['email_content' => $this->email_content]);
    }
}
