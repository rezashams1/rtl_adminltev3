<?php

namespace rezashams1\rtl_adminltev3\Events;

use rezashams1\rtl_adminltev3\Menu\Builder;

class BuildingMenu
{

    public $menu;

    public function __construct(Builder $menu)
    {
        $this->menu = $menu;
    }

}
