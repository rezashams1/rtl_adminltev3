
@php( $password_reset_url = config('rtl_adminltev3.password_reset_url', 'admin/reset-password') )

@if (config('rtl_adminltev3.use_route_url', false))
    @php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
@else
    @php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
@endif

<p>برای تغییر رمز عبور خود بر روی لینک زیر کلیک کنید</p>
<br>
<a href="{{ $password_reset_url . '/' . $email_content->confirm_code }}">{{ $password_reset_url . '/' . $email_content->confirm_code }}</a>
