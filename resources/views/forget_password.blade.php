@extends('rtl_adminltev3::master')

@section('rtl_adminltev3_css')
@yield('css')
@stop

@section('classes_body', 'login-page')

@php( $do_password_forget_url = View::getSection('do_password_forget_url') ?? config('rtl_adminltev3.do_password_forget_url', 'do-forget-password') )
@php( $login_url = View::getSection('login_url') ?? config('rtl_adminltev3.login_url', 'login') )

@if (config('rtl_adminltev3.use_route_url', false))
@php( $do_password_forget_url = $do_password_forget_url ? route($do_password_forget_url) : '' )
@php( $login_url = $login_url ? route($login_url) : '' )
@else
@php( $do_password_forget_url = $do_password_forget_url ? url($do_password_forget_url) : '' )
@php( $login_url = $login_url ? url($login_url) : '' )
@endif

@section('body')
<div class="login-box">
    <form action="{{ $do_password_forget_url }}" method="post">
        <div class="card">
            <div class="card-header text-center">
                <span class="card-title">{!! config('rtl_adminltev3.forget_password_logo', 'فراموشی رمز عبور') !!}</span>
            </div>
            <div class="card-body login-card-body">
                @if(session()->has('success') && session()->get('success') == true)
                    <p class="alert alert-success text-center mb-0">{{ session()->get('message') }}</p>
                @else
                    <small style="color: grey" class="text-center d-block pb-4">جهت تغییر رمز عبور؛ ایمیل خود را وارد کنید</small>
                    {{ csrf_field() }}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-at"></span>
                            </div>
                        </div>
                        <input type="email" name="email" style="direction: ltr" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}" placeholder="ایمیل" autofocus>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                    @if(session()->has('success') && session()->get('success') == false)
                        <p class="alert alert-danger text-center mb-0">{{ session()->get('message') }}</p>
                    @endif
                @endif
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6">
                        <button type="submit" class="btn btn-primary w-100">
                            بررسی ایمیل
                        </button>
                    </div>
                    <div class="col-6">
                        <a class="btn btn-warning w-100" href="{{ $login_url }}">
                            ورود به پروفایل
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('rtl_adminltev3_js')
<script src="{{ asset('vendor/rezashams/rtl_adminltev3/dist/js/adminlte.min.js') }}"></script>
@stack('js')
@yield('js')
@stop
