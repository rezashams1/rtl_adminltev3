@extends('rtl_adminltev3::master')

@section('rtl_adminltev3_css')
    @stack('css')
    @yield('css')
@stop

@section('classes_body',
    (config('rtl_adminltev3.sidebar_mini', true) === true ?
        'sidebar-mini ' :
        (config('rtl_adminltev3.sidebar_mini', true) == 'md' ?
         'sidebar-mini sidebar-mini-md ' : '')
    ) .
    (config('rtl_adminltev3.layout_topnav') || View::getSection('layout_topnav') ? 'layout-top-nav ' : '') .
    (config('rtl_adminltev3.layout_boxed') ? 'layout-boxed ' : '') .
    (!config('rtl_adminltev3.layout_topnav') && !View::getSection('layout_topnav') ?
        (config('rtl_adminltev3.layout_fixed_sidebar') ? 'layout-fixed ' : '') .
        (config('rtl_adminltev3.layout_fixed_navbar') === true ?
            'layout-navbar-fixed ' :
            (isset(config('rtl_adminltev3.layout_fixed_navbar')['xs']) ? (config('rtl_adminltev3.layout_fixed_navbar')['xs'] == true ? 'layout-navbar-fixed ' : 'layout-navbar-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_navbar')['sm']) ? (config('rtl_adminltev3.layout_fixed_navbar')['sm'] == true ? 'layout-sm-navbar-fixed ' : 'layout-sm-navbar-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_navbar')['md']) ? (config('rtl_adminltev3.layout_fixed_navbar')['md'] == true ? 'layout-md-navbar-fixed ' : 'layout-md-navbar-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_navbar')['lg']) ? (config('rtl_adminltev3.layout_fixed_navbar')['lg'] == true ? 'layout-lg-navbar-fixed ' : 'layout-lg-navbar-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_navbar')['xl']) ? (config('rtl_adminltev3.layout_fixed_navbar')['xl'] == true ? 'layout-xl-navbar-fixed ' : 'layout-xl-navbar-not-fixed ') : '')
        ) .
        (config('rtl_adminltev3.layout_fixed_footer') === true ?
            'layout-footer-fixed ' :
            (isset(config('rtl_adminltev3.layout_fixed_footer')['xs']) ? (config('rtl_adminltev3.layout_fixed_footer')['xs'] == true ? 'layout-footer-fixed ' : 'layout-footer-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_footer')['sm']) ? (config('rtl_adminltev3.layout_fixed_footer')['sm'] == true ? 'layout-sm-footer-fixed ' : 'layout-sm-footer-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_footer')['md']) ? (config('rtl_adminltev3.layout_fixed_footer')['md'] == true ? 'layout-md-footer-fixed ' : 'layout-md-footer-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_footer')['lg']) ? (config('rtl_adminltev3.layout_fixed_footer')['lg'] == true ? 'layout-lg-footer-fixed ' : 'layout-lg-footer-not-fixed ') : '') .
            (isset(config('rtl_adminltev3.layout_fixed_footer')['xl']) ? (config('rtl_adminltev3.layout_fixed_footer')['xl'] == true ? 'layout-xl-footer-fixed ' : 'layout-xl-footer-not-fixed ') : '')
        )
        : ''
    ) .
    (config('rtl_adminltev3.sidebar_collapse') || View::getSection('sidebar_collapse') ? 'sidebar-collapse ' : '') .
    (config('rtl_adminltev3.right_sidebar') && config('rtl_adminltev3.right_sidebar_push') ? 'control-sidebar-push ' : '') .
    config('rtl_adminltev3.classes_body')
)

@section('body_data',
(config('rtl_adminltev3.sidebar_scrollbar_theme', 'os-theme-light') != 'os-theme-light' ? 'data-scrollbar-theme=' . config('rtl_adminltev3.sidebar_scrollbar_theme')  : '') . ' ' . (config('rtl_adminltev3.sidebar_scrollbar_auto_hide', 'l') != 'l' ? 'data-scrollbar-auto-hide=' . config('rtl_adminltev3.sidebar_scrollbar_auto_hide')   : ''))

@php( $logout_url = View::getSection('logout_url') ?? config('rtl_adminltev3.logout_url', 'logout') )
@php( $dashboard_url = View::getSection('dashboard_url') ?? config('rtl_adminltev3.dashboard_url', 'home') )

@if (config('rtl_adminltev3.use_route_url', false))
    @php( $logout_url = $logout_url ? route($logout_url) : '' )
    @php( $dashboard_url = $dashboard_url ? route($dashboard_url) : '' )
@else
    @php( $logout_url = $logout_url ? url($logout_url) : '' )
    @php( $dashboard_url = $dashboard_url ? url($dashboard_url) : '' )
@endif

@section('body')

    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand {{ config('rtl_adminltev3.classes_topnav', 'navbar-dark') }}">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                </li>
            </ul>
            @yield('main_nav_left_items')
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar {{ config('rtl_adminltev3.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">
            <!-- Brand Logo -->
            <a href="#" class="brand-link {{ config('rtl_adminltev3.classes_brand', '') }}">
                <span class="brand-text font-weight-bold">@yield('logo', config('rtl_adminltev3.logo', 'پنل ادمین'))</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar" style="direction: ltr">
                <div style="direction: rtl">

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column {{ config('rtl_adminltev3.classes_sidebar_nav', '') }}" data-widget="treeview" role="menu" data-accordion="false">

                            @each('rtl_adminltev3::menu', $rtl_adminltev3->menu(), 'item')

                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->

                </div>
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    @yield('main_content')

                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <strong>@yield('footer_content', 'پنل ادمین')</strong>
        </footer>

    </div>

@stop

@section('rtl_adminltev3_js')
    @stack('js')
    @yield('js')
@stop
