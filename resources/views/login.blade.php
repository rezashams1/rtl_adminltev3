@extends('rtl_adminltev3::master')

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

@section('classes_body', 'login-page')

@php( $login_url = View::getSection('do_login_url') ?? config('rtl_adminltev3.do_login_url', 'rtl_adminltev3/do-login') )
@php( $password_forget_url = View::getSection('password_forget_url') ?? config('rtl_adminltev3.password_forget_url', 'forget-password') )

@if (config('rtl_adminltev3.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $password_forget_url = $password_forget_url ? route($password_forget_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $password_forget_url = $password_forget_url ? url($password_forget_url) : '' )
@endif

@section('body')
    <div class="login-box">
        <form action="{{ $login_url }}" method="post" autocomplete="off">
            <div class="card">
                <div class="card-header text-center">
                    <span class="card-title">{!! config('rtl_adminltev3.login_logo', 'ورود به پنل ادمین') !!}</span>
                </div>
                <div class="card-body login-card-body">
                    <small style="color: grey" class="text-center d-block pb-4">جهت ورود به پنل ادمین؛ ایمیل و رمز عبور خود را وارد کنید</small>
                    {{ csrf_field() }}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-at"></span>
                            </div>
                        </div>
                        <input type="email" name="email" style="direction: ltr" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="ایمیل" autocomplete="off">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-lock"></span>
                            </div>
                        </div>
                        <input type="password" name="password" style="direction: ltr" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="رمز عبور" autocomplete="off">
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary w-100">
                                ورود
                            </button>
                        </div>
                        <div class="col-6">
                            <a class="btn btn-warning w-100" href="{{ $password_forget_url }}">
                                فراموشی رمز عبور
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('rtl_adminltev3_js')
    <script src="{{ asset('vendor/rezashams/rtl_adminltev3/dist/js/adminlte.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
