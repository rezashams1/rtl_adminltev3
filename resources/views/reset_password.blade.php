@extends('rtl_adminltev3::master')

@section('rtl_adminltev3_css')
    @yield('css')
@stop

@section('classes_body', 'login-page')

@php( $do_password_reset_url = View::getSection('do_password_reset_url') ?? config('rtl_adminltev3.do_password_reset_url', 'do-reset-password') )
@php( $dashboard_url = View::getSection('dashboard_url') ?? config('rtl_adminltev3.dashboard_url', 'dashboard') )
@php( $login_url = View::getSection('login_url') ?? config('rtl_adminltev3.login_url', 'login') )

@if (config('rtl_adminltev3.use_route_url', false))
    @php( $do_password_reset_url = $do_password_reset_url ? route($do_password_reset_url) : '' )
    @php( $dashboard_url = $dashboard_url ? route($dashboard_url) : '' )
    @php( $login_url = $login_url ? route($login_url) : '' )
@else
    @php( $do_password_reset_url = $do_password_reset_url ? url($do_password_reset_url) : '' )
    @php( $dashboard_url = $dashboard_url ? url($dashboard_url) : '' )
    @php( $login_url = $login_url ? url($login_url) : '' )
@endif

@section('body')
    <div class="login-box">
        <form action="{{ $do_password_reset_url }}" method="post">
            <div class="card">
                <div class="card-header text-center">
                    <span class="card-title">{!! config('rtl_adminltev3.reset_password_logo', 'تغییر رمز عبور') !!}</span>
                </div>
                <div class="card-body login-card-body">
                    @if (isset($errors->invalid_code))
                        <p class="alert alert-danger text-center mb-0">{{ $errors->invalid_code }}</p>
                    @else
                        @if(session()->has('success') && session()->get('success') == true)
                            <p class="alert alert-success text-center">{{ session()->get('message') }}</p>
                        @else
                            <small style="color: grey" class="text-center d-block pb-4">رمز عبور جدید خود را وارد کنید</small>
                            {{ csrf_field() }}
                            <input name="user_id" value="{{ $user_id }}" style="display: none">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <span class="far fa-lock"></span>
                                    </div>
                                </div>
                                <input type="password" name="password" style="direction: ltr" class="form-control {{ isset($errors->password) ? 'is-invalid' : '' }}" placeholder="رمز عبور جدید" autofocus>
                                @if (isset($errors->password))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->password }}</strong>
                                    </div>
                                @endif
                            </div>
                            @if(session()->has('success') && session()->get('success') == false)
                                <p class="alert alert-danger text-center mb-0">{{ session()->get('message') }}</p>
                            @endif
                        @endif
                    @endif
                </div>
                <div class="card-footer">
                    @if(session()->has('success') && session()->get('success') == true)
                        <div class="row">
                            <div class="col-12">
                                <a class="btn btn-warning w-100" href="{{ $login_url }}">
                                    ورود به پروفایل
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary w-100">
                                    تغییر رمز عبور
                                </button>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-warning w-100" href="{{ $login_url }}">
                                    ورود به پروفایل
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
@stop

@section('rtl_adminltev3_js')
    <script src="{{ asset('vendor/rezashams/rtl_adminltev3/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
