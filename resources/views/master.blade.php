<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        @yield('title_prefix', config('rtl_adminltev3.title_prefix', ''))
        @yield('title', config('rtl_adminltev3.title', 'پنل ادمین'))
        @yield('title_postfix', config('rtl_adminltev3.title_postfix', ''))
    </title>
    <meta name="description" content="@yield('description')"></meta>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/morris/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/css/custom-style.css') }}">

    @yield('css')

</head>

<body class="@yield('classes_body')" style="@yield('styles_body')" @yield('body_data')>

@yield('body')

<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/js/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/js/plugins/jquery/raphael-min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/knob/jquery.knob.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/js/plugins/jquery/moment.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ asset('/vendor/rezashams/rtl_adminltev3/dist/js/adminlte.js') }}"></script>
<script>
    $(function () {

    })
</script>

@yield('js')

</body>
</html>
