### **Laravel AdminLTE V3 Package Modified For RTL Support**

#### Requirements

- Laravel > 6.0
- PHP > 7.2

#### **Installation**

First create your database and add the required information in your .env file. then simply run the command below inside your laravel project :

`composer require rezashams/rtl_adminltev3`

Then run the command below :

`php artisan publish:vendor`

Finally run the command below :

`php artisan migrate`

This will add the package to your project and do the required stuff for you.

#### Notices

- make sure you dont have database table named `admin_users`
- make sure you dont have database table named `admin_users_activations`
- make sure you dont have auth guard named `admin` in your project
- make sure you don't have auth provider named `admins` in your project
